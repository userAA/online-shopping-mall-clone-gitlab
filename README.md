gitlab page https://gitlab.com/userAA/online-shopping-mall-clone-gitlab.git
gitlab online-shopping-mall-clone-gitlab

проект online-shopping-mall-clone (интернет магазин на react и redux)
используемые технологии на фронтенде:
    antd,
    axios,
    formik,
    moment,
    react,
    react-dom,
    react-dropzone,
    react-icons,
    react-image-gallery,
    react-paypal-express-checkout,
    react-redux,
    react-router-dom,
    react-scripts,
    redux,
    redux-form,
    redux-promise,
    redux-thunk,
    socket.io-client,
    yup.

используемые технологии на бекэнде:
    async,
    bcrypt,
    bcryptjs,
    body-parser,
    cookie-parser,
    express,
    jsonwebtoken,
    mongoose,
    multer,
    socket.io.

Имеется регистрация и авторизация пользователя. Любой авторизованный пользователь формирует товары по фотографии,
названию, описанию, цене и стране изгоовления. Все товары от всех пользователей с их изображениями помещаются на 
центральную страницу проекта. На центральной странице проекта авторизованный пользователь может формировать корзину товаров
нажимая на изображение каждого товара и отправляя этот товар или несколько его единиц в собственную корзину товаров.
В корзине товаров авторизованный пользователь может удалять любые товары на свое усмотрение.