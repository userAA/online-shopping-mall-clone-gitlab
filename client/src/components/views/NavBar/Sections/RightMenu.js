/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import Icon from '@ant-design/icons';
import { Menu, Badge } from 'antd';
import axios from 'axios';
import { USER_SERVER } from '../../../Config';
import { withRouter } from 'react-router-dom';
import { useSelector } from "react-redux";

function RightMenu(props) {
  //вытаскиваем из store всю информацию об авторизованном пользователе
  const user = useSelector(state => state.user)

  const logoutHandler = () => {
    //запрос на очистку данных авторизованного пользователя
    axios.get(`${USER_SERVER}/logout`).then(response => {
      if (response.status === 200) {
        //переход на страницу авторизации пользователя
        props.history.push("/login");
      } else {
        //очистка данных авторизованного пользователя не прошла
        alert('Log Out Failed')
      }
    });
  };

  if (user.userData && !user.userData.isAuth) {
    //случай, когда пользователь неавторизован или незарегистрирован
    return (
      <Menu mode={props.mode}>
        {/*Переход на страницу авторизации пользователя */}
        <Menu.Item key="mail">
          <a href="/login">Signin</a>
        </Menu.Item>
        {/*Переход на страницу регистрации пользователя */}
        <Menu.Item key="app">
          <a href="/register">Signup</a>
        </Menu.Item>
      </Menu>
    )
  } else {
    return (
      <Menu mode={props.mode}>
        {/*Переход на страницу формирования нового товара по фотографии авторизованным пользователем */}
        <Menu.Item key="upload">
          <a href="/product/upload">Upload</a>
        </Menu.Item>

        {/*Переход на страницу показа корзины товаров, выбранных авторизованным пользователем */}
        <Menu.Item key="cart">
          {/*user.userData.cart.length количество товаров в корзине */}
          <Badge count={user.userData && user.userData.cart.length}>
            <a href="/user/cart" style={{marginRight: -22, color: "#667777"}}>
              <Icon type="shopping-cart" style={{fontSize: 30, marginBottom: 4}}/>
            </a>
          </Badge>
        </Menu.Item>

        {/*Переход на страницу очистки данных по авторизованному пользователю */}
        <Menu.Item key="logout">
          <a onClick={logoutHandler}>Logout</a>
        </Menu.Item>
      </Menu>
    )
  }
}

export default withRouter(RightMenu);