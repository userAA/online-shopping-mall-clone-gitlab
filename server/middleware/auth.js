import User from "../models/User.js";

//функция проверки существования авторизованного пользователя
let auth = (req, res, next) => {  
  //определяем токен сформированный в cookies 
  let token = req.cookies.w_auth;

  //в базе данных на сервере находим определенный токен
  User.findByToken(token, (err, user) => 
  {
    //случилась ошибка непредвиденная
    if (err) throw err;

    //пользователя нет тогда отправляем на клиент-приложение отрицательный результат
    if (!user) return res.json({isAuth: false, error: true});

    //отправляем в маршрутизаторы токен и самого пользователя, который ему соответствует  
    req.token = token;
    req.user = user;
    next();
  });  
};

export default auth;